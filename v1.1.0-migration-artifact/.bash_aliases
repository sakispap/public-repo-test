# Aliases definiton below
echo -e "-----------------------------------------------------"
echo -e "Aliases:"
echo -e "> sysum: Display system information summary"
echo -e "> scanlog: Display scanner log (40 last lines)"
echo -e "> scanlog100: Display last 100 lines of scanner log"
echo -e "> scanlog1000: Display last 1000 lines of scanner log"
echo -e "> scanlogf: Follow scanner log"

# Aliases below
alias sysum="bash /vieconnect/scanner/scripts/sysum.sh"
alias scanlog="journalctl -u scanner -n 40"
alias scanlog100="journalctl -u scanner -n 100"
alias scanlog1000="journalctl -u scanner -n 1000"
alias scanlogf="journalctl -u scanner -f"
